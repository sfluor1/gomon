# [Gomon](https://asciinema.org/a/P47l9RULMatxo72QnRseH5bCo)

Gomon is my work for the **Datadog** assignment. The subject was to develop a configurable website monitoring-tool.
You can find the structure documentation [here](./_docs/structure.md).

## Installation

You will need to have [go](https://golang.org/doc/install), [dep](https://github.com/golang/dep) and [git](https://git-scm.com/) on your computer.
To be able to run all the tests it will be easier for you to also have [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/) installed.

You can then go get the project with:

`go get gitlab.com/sfluor1/gomon`

Then navigate to the gomon directory (should be in `$GOPATH/gitlab.com/sfluor1/gomon`) and run `dep ensure` to install the project dependencies.

After that you can build the binary with `go build` and start using gomon.

## Usage

```
Usage:
  gomon [flags]
  gomon [command]

Available Commands:
  agent       Starts the gomon agent
  dashboard   Starts the gomon cli-dashboard
  help        Help about any command
  mock        Starts a mock server that returns random Status codes

Flags:
  -h, --help   help for gomon
```

The gomon cli is composed of 3 parts:

### The agent

The gomon agent is responsible for website metrics polling, alerting system and database handling.
It's the core of the gomon project.

```
Usage:
  gomon agent [flags]
  gomon agent [command]

Available Commands:
  ping        Pings the gomon agent

Flags:
  -c, --config string   Path to the gomon configuration file
  -h, --help            help for agent
  -l, --log string      Path to a log file if you want to enable alerts logging
  -p, --port uint       Port on which to listen for websocket connections (default 4444)
  -r, --restrict        If restrict mode should be enabled (refuse all websocket connections without a correct Origin header)

Use "gomon agent [command] --help" for more information about a command.
```

To start it you can simply do `./gomon agent config -c ./agent/config/config.json.template` and it will start monitoring the websites listed in the config.json.template file.

If you have *docker* and *docker-compose* installed on your system you can also use `./scripts/run_agent.sh` to run the agent with the default mongodb configuration and using the port *4444*. Nevertheless it won't run the dashboard so you will have to start it aside. 

#### The configuration

The agent configuration is in a JSON format and should look like this:

```json
{
    "watchers": [ // All the websites we need to watch
        {
            "url": "https://medium.com/",
            "checkFrequency": 10
        },
        {
            "url": "https://www.datadoghq.com/",
            "checkFrequency": 10,
            "respTimeThreshold": 0.05
        }, 
        ...
    ],
    "timeout": 10, // Timeout for the requests
    "hooks": [
        "http://requestbin.fullcontact.com/1nm66q11"
    ],
    "section1": {
        "timeRange": 600,
        "refreshFreq": 10
    },
    "section2": {
        "timeRange": 3600,
        "refreshFreq": 60
    },
    "alertSection": {
        "timeRange": 120,
        "refreshFreq": 10
    },
    "db": {
        "database": "gomondb",
        "addr": "localhost:27017",
    }
}
```

The parts of the configuration are:
- the *watchers* that are websites we need to monitor. For each website we have three parameters:
- - url: The url of the website
- - checkFrequency: The interval between two checks in second
- - respTimeThreshold (optional): The response time threshold for the average response time. If the threshold is crossed it triggers an alert
- timeout: timeout in second of the check requests
- hooks: a list of url to notify in case of an alert (useful for a slack bot or to send notifications by mail). A hook alert is a post request that have the same format as the *Notification* struct defined in *agent/alerter/hooks.go*
- The sections configuration (section1, section2, alertSection), for each of them we have 2 parameters:
- - timeRange: the range in second on which to compute the metrics/alerts for this given section
- - refreshFreq: interval in second between two refresh for this section
- db (optional): Database related parameters (if not given the agent will use an in-memory database)
- - database: The name of the database to use
- - addr: The address (and port) of the server hosting the database
- - username: the username to use to connect to the database
- - password: the password of the database user 

There are 3 base configuration file in the agent/config folder:
- config.json.template: A basic configuration using an in-memory database
- mongo.config.json.template: A basic configuration using a mongodb database for persistent storage
- benchmark.config.json: A configuration to benchmark gomon by monitoring 500 websites. It have been generated with [this](https://moz.com/top500) website list. 

Note that the configuration is also used by the dashboard (its sent by the agent on the first connection) to show the time range used to compute the metrics to the user.

#### The ping command

The `gomon agent` also ships with a basic status command that pings the agent and print its uptime

For example, `gomon agent ping` returns the following:
```
Pong
Agent is up since: Wed Apr  4 17:43:21 UTC 2018
```

### The mock server

The `gomon mock` command starts a web server that returns random status codes. By default, it listens on the port *8888* defined in the config.json.template file.
The probability of sending a """down""" status code is 20%.

### The dashboard

[View the dashboard on asciinema](https://asciinema.org/a/P47l9RULMatxo72QnRseH5bCo)

The dashboard is a cli-dashboard that connects to the agent via a websocket connection and display alerts and metrics to the user.
You can navigate through panels with \<left\> and \<right\>, you can scroll down/up with \<down\>/\<up\> and you can filter by a specific string by hitting \<^s\>.
By default the dashboard tries to connect to *localhost:4444* but you can provide another address with the `-a` flag.

## Testing

The project have mainly been tested on MacOS and Linux (Debian) but should work on Windows as well.

You can test the project in three ways:

### Unit tests

There are various unit tests in the gomon packages (they are located in  `_test.go` files).
You can run all the tests by doing `./scripts/run_tests.sh`, it will start a mongodb container and a test container.

You can also run the tests by doing `go test ./...` but the tests in `mongodb_test.go` will fail by attempting to connect to find a mongodb database at `mongo:27017`.

There is a CI pipeline that run the tests at each commit so we can check the code status [here](https://gitlab.com/sfluor1/gomon/pipelines)

### Manual tests

Since unit tests are not sufficient for testing, another way of testing gomon is by running it with the mock server from `gomon mock` that will trigger multiple availability alerts to test the alerting system. It works quite well.

To check for data races in gomon we can use the `-race` flag in all go commands. It generates a detailed report of where data races happen in the code.

### Performances test

Finally we can also test the performances with [pscrecord](https://github.com/astrofrog/psrecord) for instance that will plot a graph of the resources used by the gomon agent during a given duration. 
With the benchmark configuration and a MacBook Pro (2,3 GHz Intel Core i5, 8 Go 2133 MHz LPDDR3) we get the following:

![Performance plot](./_docs/perfplot.png "Performance plot for gomon")

There is also a graph generated by `pprof` here: 

![Pprof plot](./_docs/gomonpprof.svg "Pprof plot for gomon")

We see that most of the time is passed in runtime/cgocall.go:

```
(pprof) list cgocall
Total: 1.08mins
ROUTINE ======================== runtime.cgocall in /usr/local/Cellar/go/1.10/libexec/src/runtime/cgocall.go
    18.23s     18.44s (flat, cum) 28.51% of Total
         .          .    123:   //
         .          .    124:   // fn may call back into Go code, in which case we'll exit the
         .          .    125:   // "system call", run the Go code (which may grow the stack),
         .          .    126:   // and then re-enter the "system call" reusing the PC and SP
         .          .    127:   // saved by entersyscall here.
    18.23s     18.23s    128:   entersyscall(0)
         .          .    129:
         .          .    130:   mp.incgo = true
         .          .    131:   errno := asmcgocall(fn, arg)
         .          .    132:
         .          .    133:   // Call endcgo before exitsyscall because exitsyscall may
         .          .    134:   // reschedule us on to a different M.
         .          .    135:   endcgo(mp)
         .          .    136:
         .      210ms    137:   exitsyscall(0)
         .          .    138:
         .          .    139:   // From the garbage collector's perspective, time can move
         .          .    140:   // backwards in the sequence above. If there's a callback into
         .          .    141:   // Go code, GC will see this function at the call to
         .          .    142:   // asmcgocall. When the Go call later returns to C, the
```

We notice that most of the time is spent in doing cgocalls through [this](https://github.com/golang/go/blob/master/src/net/cgo_unix.go#L138-L198) function that does a DNS lookup, so we can't really speed up this (maybe by storing the IP addresses instead of the websites urls ? but then https would cause issues).


## Dependencies

- github.com/gorilla/websocket for the websocket manipulation
- github.com/jroimartin/gocui for the dashboard user interface
- github.com/spf13/cobra to build an intuitive CLI
- gopkg.in/mgo.v2 to interact with a mongodb database

And their dependencies.

## Possible Improvements

One of the improvements could be to add a data retention policy to avoid storing data that isn't relevant (for metrics). It will increase the performances in a long run and decrease the need of storage. 
Always in the data aspect, we could use a more relevant data storage system specialized in time series like InfluxDB or Graphite that will embed its own data policy and query functions (to compute the min, max and average response time in a more optimized way for instance).

In terms of configuration, we could use default values instead of asking the user to complete the configuration file, it will be more friendly for the users since it will give them an indication of a "good value" for a given parameter. We could also had a parameter to indicate if we want to follow redirects or not per client and globally. Indeed, here the decision have been made of not following redirects but it could be interesting to follow them too and to store metrics for each request made during the redirect. An other good thing to have would be an interactive shell with the agent to change the configuration dynamically.

Moreover, for a more scalable architecture we could use a task queue, a message queue and multiple separated agents. Each agent would take a task from the task queue (that will be the necessity of monitoring a specific list of websites) and will send the metrics to the message queue. Then an other pool of alerter/notifier would take messages from the message queue and store them in a database and send alerts to the clients and hooks if needed.

We could also add a web API or a GRPC server to provide another way of interacting with the agent since websockets are not well suited for every purpose.

Finally we could do a web based user interface that would be more user-friendly and prettier than the current CLI based dashboard
