## Subject

### Overview

- Create a console program to monitor performance and availability of websites
- Websites and check intervals are user defined
- Users can keep the console app running and monitor the websites

### Stats

- Check the different websites with their corresponding check intervals
- - Compute a few interesting metrics: availability, max/avg response times, response codes count and more...
- - Over different timeframes: 2 minutes and 10 minutes
- Every 10s, display the stats for the past 10 minutes for each website
- Every minute, displays the stats for the past hour for each website

### Alerting

- When a website availability is below 80% for the past 2 minutes, add a message saying that "Website {website} is down. availability={availablility}, time={time}"
- When availability resumes for the past 2 minutes, add another message detailing when the alert recovered
- Make sure all messages showing when alerting thresholds are crossed remain visible on the page for historical reasons

### Tests & question

- Write a test for the alerting logic
- Explain how you'd improve on this application design

## Personal notes

### Remarks

Pinging the website to check its availability is not a good idea since the website can be down with its server being up

### How to keep websites and check intervals

With a configuration file toml, yaml or json.
JSON seems to be the most known format.
Check that the websites given are correct websites ?
It will also store the database configuration

Put timeout in the configuration too 

### Interesting metrics

#### Instant metrics 

- availability
- response time
- response code
- check https ?

#### Computed metrics

- max/min/avg response time
- response code count
- uptime/downtime

### How to get the metrics

By doing a get request to the URL

### How to store values ?

- SQLite -> Easy to setup but not suited for time series
- InfluxDB -> Easy to setup with a docker environment and suited for time series
- Graphite -> ?
- InMemory option 

### Add an option to store the logs 

A double writer, that writes both to the Stdout and the log file (if given as option)
The logs shouldn't be overwritten if the program is relaunched

### Check periodically ?

A go routine

### How to manage alerting

A field in the DB for a given website that is set to alerting if a given url is not reachable

### How to test the alerting logic

### Extras

#### Alerts that call a specific url with data

#### Web-API

#### Frontend