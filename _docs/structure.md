# Project structure

![Project structure](./structuregraph.jpg "Graph structure of the project")

The project is divided into 8 packages:
- The agent package that "comports":
    - The alerter package
    - The config package
    - The db package
    - The logger package
    - The watcher package
- The dashboard package
- The cmd package

## The agent package

The agent package goal is to pack all the previously described packages and a WebSocket server to interact with the clients.
Let's first dive in all the agent "sub-packages":

### The config package

The config package simply parses a config file in json format and returns a `GomonConfig` that represents the configuration.
An example of config can be found in agent/config/config.json.template and looks like the following:

```json
// All the time units are in second. 
{
    "watchers": [ // All the websites we need to watch
        {
            "url": "https://medium.com/",
            "checkFrequency": 10
        },
        {
            "url": "https://www.datadoghq.com/",
            "checkFrequency": 10,
            "respTimeThreshold": 0.05
        }, 
        ...
    ],
    "timeout": 10, // Timeout for the requests
    "hooks": [
        "http://requestbin.fullcontact.com/1nm66q11"
    ],
    "section1": {
        "timeRange": 600,
        "refreshFreq": 10
    },
    "section2": {
        "timeRange": 3600,
        "refreshFreq": 60
    },
    "alertSection": {
        "timeRange": 60,
        "refreshFreq": 5
    }
}

```

### The alerter package

The alerter package is responsible for triggering alerts when needed for both Availability and Response Time (if defined in the configuration file) but it also includes a notifier that computes and notify the clients of the latest metrics (like Average, Min, Max Response Times) by performing a lookup in the database.

It has a `Check` function that regularly (the frequency is defined in the configuration file in the "alertSection" field) checks for possible alerts. 

Each time an alert is detected (Availability or Response Time):
- An alert message is sent to the hooks url specified in the configuration file via a POST request
- The alert message is passed to the Logger to log it to the Stdout and/or to the log file provided by the user
- The message is sent to the agent broadcast channel (for further use by the agent server)

(Remark: Even if there are no alerts detected, an empty message is sent to the broadcast channel to notify clients that there were no alerts)

### The db package

The db package defines an interface that models the database, it should implement the following actions:

```go
// Database is an interface modeling the database
type Database interface {
	AddRecord(point DataPoint)
	Get(url string) (WebRecord, bool)
	SetRTAlert(url string, state bool, respTime float64) AlertRecord       // Set Response Time Alert
	SetAvblAlert(url string, state bool, availability float64) AlertRecord // Set Availability Alert
	GetRecord(url string, since time.Duration, now time.Time) ([]DataPoint, bool)
	GetAllRecords() map[string]*WebRecord
	GetAllAlerts() []AlertRecord
}
```

It implements this interface with a simple in-memory database based on a hashmap but also with a mongodb client.
To ensure thread safety, the in-memory database struct also has a mutex.

The data is added to the database with the `HandleDataFlow` function that simply listen on a DataPoint channel fed by the watcher (in the watcher package).

### The watcher package

The watcher package is responsible for retrieving data about websites and to store those data in its `Data` channel.
This `Data` channel is then used by the database `HandleDataFlow` function.

The data for a given website are collected with a simple get request on the URL through the `collect` function:
- To get the response time we record how long took the call to `client.Get`
- We get the status code by reading the request
- To get the website state we check if there was no error during the request and if the status code is of type 1XX, 2XX or 3XX

The method used by the agent is the `Watch` method that simply starts a go routine for each website where `collect` is called at the frequency specified in the configuration file.

Remarks:
- The choice have been made to not follow redirections because it induced a lot more traffic load (thus altering performances)
- The decision method to decide whether a website is up or not is quite simple but not really accurate, we could check the status code in a smarter way. For example, the website could send a 429 Too many request code which will be counted as a wrong "Down point"
- We could also retrieve other metrics like the Transport time or the dial time
- Spawning a go routine per website is performing nicely for a small number of websites but it would be better to use workers with a task channel to ensure that the go runtime doesn't spend too much time on go routine handling.

### The agent package

The agent package have a `Start`method to start the agent that does the following:
- Spawn a go routine for the database `HandleDataFlow` function
- Spawn a go routine for the alerter `Check` method
- Spawn a go routine for the watcher `Watch` method
- Spawn a go routine for the notifier `Notify` method
- Spawn a go routine to handle the websocket messages through the different channels (described below)
- And finally start a web server on the given port

Remark: Since there are numerous go routines that interacts with each others we should worry about possible data races.
Thanks to the `-race` flag we can pass through go commands we can detect them, and at this moment there is no data races in the agent package.

#### The websocket server

The websocket server is defined in the agent/websocket.go file and contains the following:
- A broadcast channel where messages are sent by the notifier and the alerter
- A register channel to handle new clients connections
- An unregister channel to handle clients disconnections
- A map to store all the connections as custom connection type (mandatory to be able to send broadcast messages)
- A cache where the last computed metrics and the list of the previous alerts are stored (useful to show previous data instantly to a new connected client)

The custom connection type contains:
- A websocket connection
- A send buffered channel that stores the messages that needs to be sent to the client. The max number of pending message value is 2048 (so if 2049 messages are in queue for this client, we can think that's because the client is disconnected so we send the connection to the unregister channel for further disconnection)

When a new client wants to connect to the agent it needs to request '/' route handle by the `wsHandler` that will do the following:
- Transform the HTTP connection into a Websocket connection
- Send the configuration to the client
- Send the previous alerts and the last computed metrics (stored in the server cache)
- Create a new custom type connection for the client
- Spawn a go routine to listen on the specific client send channel (if an error happens or the chan is closed the go routine exits and the client connection is sent to the unregister channel)

To handle all those channels there is the `run` method that listen on:
- register, to register new clients in the hashmap
- unregister, to unregister clients from the hashmap
- broadcast, to cache the message and push it to all the clients send channels
- sigs, for graceful shutdown (if a SIGINT or a SIGTERM signals are sent to the agent, all the connections are closed)

## The dashboard package

The dashboard is responsible for displaying the agent information (metrics and alerts) to the user.

It connects to the agent through a websocket and keeps listening for new messages through the `update` and `handleMessage`methods defined in dashboard/update.

It contains 3 sections/Tabs:
- Section 1 that displays metrics for a first time range and refresh frequency 
- Section 2 that displays metrics for a second time range and refresh frequency 
- Alerts section that displays all the alerts

The configuration for the time ranges is retrieved during the first websocket connection to the client along with the previous alerts and the last computed metrics.

It also have a simple search field that allow the user to filter the information displayed on the screen.

Colors are printed to the terminal thanks to ANSI codes.

## The cmd package

The cmd package defines the command line programs for gomon.
It uses the [cobra package](https://github.com/spf13/cobra) to define the available commands.

It also comports the mock server that allow us to answer requests with random status codes.
The status codes are take in the \[100, 475\] interval allowing us to send "bad" status codes 20% of the time and so to trigger as much alerts as possible.