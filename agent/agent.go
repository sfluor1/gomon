package agent

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"gitlab.com/sfluor1/gomon/agent/alerter"
	"gitlab.com/sfluor1/gomon/agent/config"
	"gitlab.com/sfluor1/gomon/agent/db"
	"gitlab.com/sfluor1/gomon/agent/logger"
	"gitlab.com/sfluor1/gomon/agent/watcher"
)

// Agent models the agent whole structure
type Agent struct {
	database  db.Database
	alerter   *alerter.Alerter
	watcher   *watcher.Watcher
	notifier  *alerter.Notifier
	config    config.GomonConfig
	server    *server // HTTP server that also handles the websocket connections
	restrict  bool
	startTime time.Time // Start time of the agent
}

// New Creates a new Agent with the provided configuration file
func New(config config.GomonConfig, logFile *os.File, restricted bool) *Agent {
	watcher := watcher.New(config)
	al := alerter.New(logger.New(os.Stdout, logFile), config)
	database := db.New(config.DB)
	not := &alerter.Notifier{Section1: config.Section1, Section2: config.Section2}

	return &Agent{
		database,
		al,
		watcher,
		not,
		config,
		&server{make(chan alerter.Message), make(chan *connection), make(chan *connection), map[*connection]bool{}, cache{}},
		restricted,
		time.Now(),
	}
}

// Start starts the agent and launches all the necessary parts to monitor the websites
func (a *Agent) Start(port uint) {

	// Store all the received metrics to the Database
	go db.HandleDataFlow(a.database, a.watcher.Data)

	// Perform the alert checks
	go a.alerter.Check(a.database, a.server.broadcast)

	// Start watching the websites
	go a.watcher.Watch()

	// Notify clients of metrics change
	go a.notifier.Notify(a.database, a.server.broadcast)

	// Handle broadcast, register, unregister and signals channels
	go a.run()

	// Start the websocket server
	http.HandleFunc("/", a.wsHandler)
	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Pong\nAgent is up since: %s", a.startTime.Format(time.UnixDate))
	})

	fmt.Println("Starting agent...")
	panic(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
