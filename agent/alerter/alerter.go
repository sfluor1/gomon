package alerter

import (
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"
	"gitlab.com/sfluor1/gomon/agent/db"
	"gitlab.com/sfluor1/gomon/agent/logger"
)

// Alerter performs checks by looking in the database
type Alerter struct {
	Lg                 *logger.Logger     // Lg is the logger used for the alerts
	Hooks              []string           // A list of url to contact in case of alert
	RespTimeThresholds map[string]float64 // List of the threshold for response time alerts
	TimeRange          time.Duration      // Time range on which to compute metrics for alert lookup (default to 600s)
	refreshFreq        time.Duration      // Refresh frequency for alerts (default to 10s)
}

// New creates a new Alerter
func New(lg *logger.Logger, conf config.GomonConfig) *Alerter {
	a := &Alerter{
		lg,
		conf.Hooks,
		map[string]float64{},
		time.Duration(conf.AlertSection.TimeRange) * time.Second,
		time.Duration(conf.AlertSection.RefreshFreq) * time.Second,
	}

	// Default values for time range and refresh frequency
	if conf.AlertSection.TimeRange == 0 {
		a.TimeRange = 600 * time.Second // 10mn
	}

	if conf.AlertSection.RefreshFreq == 0 {
		a.refreshFreq = 10 * time.Second // 10s
	}

	// Parse response time thresholds
	for _, wc := range conf.Watchers {
		if wc.RespTimeThreshold != 0 {
			a.RespTimeThresholds[wc.URL] = wc.RespTimeThreshold
		}
	}
	return a
}

func alrtToMsg(al db.AlertRecord) Message {
	if al.URL != "" {
		return Message{Type: ALRTSECTIONUPDATE, Alerts: []db.AlertRecord{al}, TimeStamp: time.Now()}
	}

	// If there is no alert we still send an empty message to notify that there were no alerts
	return Message{Type: ALRTSECTIONUPDATE, Alerts: []db.AlertRecord{}, TimeStamp: time.Now()}
}
