package alerter

import (
	"testing"
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"
	"gitlab.com/sfluor1/gomon/agent/db"
	"gitlab.com/sfluor1/gomon/agent/logger"
)

// A mock writer
type fakeWriter struct{}

func (w fakeWriter) Write(p []byte) (n int, err error) { return len(p), nil }
func TestCheckOneAvailability(t *testing.T) {
	now := time.Now()

	tt := map[string]struct {
		wr       *db.WebRecord
		expected bool
		desc     string
	}{
		"1": {&db.WebRecord{}, false, "empty record"},
		"2": {
			&db.WebRecord{Points: []db.DataPoint{
				db.DataPoint{Up: true, TimeStamp: now},
				db.DataPoint{Up: true, TimeStamp: now},
				db.DataPoint{Up: true, TimeStamp: now},
				db.DataPoint{TimeStamp: now},
				db.DataPoint{TimeStamp: now},
			}},
			true,
			"3/5 of availability",
		},
		"3": {
			&db.WebRecord{AvblAlerting: true},
			false,
			"empty record with alerting set to true",
		},
		"4": {
			&db.WebRecord{Points: []db.DataPoint{
				db.DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{TimeStamp: now},
			}},
			true,
			"5/6 of availability but over the last hour only (0 over the last 2 minutes)",
		},
		"5": {
			&db.WebRecord{Points: []db.DataPoint{db.DataPoint{Up: true, TimeStamp: now}}, AvblAlerting: true},
			false,
			"Non alerting record with alerting set to true",
		},
	}

	al := New(logger.New(fakeWriter{}, nil), config.GomonConfig{AlertSection: config.Section{TimeRange: 10}})

	for key, tc := range tt {
		ch := make(chan Message)
		go func() { <-ch }()
		database := db.New(config.DB{})

		for _, p := range tc.wr.Points {
			p.URL = key
			database.AddRecord(p)
		}
		database.SetAvblAlert(key, tc.wr.AvblAlerting, db.GetAvailability(database, key, 5*time.Minute))

		if res := al.checkOneAvailability(key, database, now, ch); res != tc.expected {
			t.Errorf("Test n°%v (%s) failed, expected %t but got %t", key, tc.desc, tc.expected, res)
		}
	}
}

func TestCheckOneRespTime(t *testing.T) {
	now := time.Now()

	thresholds := map[string]float64{"1": 0.5, "2": 0.5, "3": 0.5, "4": 0.5, "5": 0.5}

	tt := map[string]struct {
		wr       *db.WebRecord
		expected bool
		desc     string
	}{
		"1": {&db.WebRecord{}, false, "empty record"},
		"2": {
			&db.WebRecord{Points: []db.DataPoint{
				db.DataPoint{Up: true, ResponseTime: 0.4, TimeStamp: now},
				db.DataPoint{Up: true, ResponseTime: 0.6, TimeStamp: now},
				db.DataPoint{Up: true, ResponseTime: 0.8, TimeStamp: now},
				db.DataPoint{Up: true, ResponseTime: 0.9, TimeStamp: now},
				db.DataPoint{Up: true, ResponseTime: 0.3, TimeStamp: now},
			}},
			true,
			"Critical (mean is 0.6)",
		},
		"3": {
			&db.WebRecord{RTAlerting: true},
			false,
			"empty record with alerting set to true",
		},
		"4": {
			&db.WebRecord{Points: []db.DataPoint{
				db.DataPoint{Up: true, ResponseTime: 0.04, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{Up: true, ResponseTime: 0.04, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{ResponseTime: 0.04, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{ResponseTime: 0.04, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{ResponseTime: 0.04, TimeStamp: now.Add(-time.Hour)},
				db.DataPoint{Up: true, ResponseTime: 0.8, TimeStamp: now},
			}},
			true,
			"good response time but over the last hour only (0.8 over the last 2 minutes)",
		},
		"5": {
			&db.WebRecord{Points: []db.DataPoint{db.DataPoint{ResponseTime: 0.01, TimeStamp: now}}, RTAlerting: true},
			false,
			"Non alerting record with alerting set to true",
		},
	}

	al := New(logger.New(fakeWriter{}, nil), config.GomonConfig{AlertSection: config.Section{TimeRange: 10}})

	al.RespTimeThresholds = thresholds

	for key, tc := range tt {
		ch := make(chan Message)
		go func() { <-ch }()
		database := db.New(config.DB{})
		for _, p := range tc.wr.Points {
			p.URL = key
			database.AddRecord(p)
		}
		if res := al.checkOneRespTime(key, database, now, ch); res != tc.expected {
			t.Errorf("Test n°%v (%s) failed, expected %t but got %t", key, tc.desc, tc.expected, res)
		}
	}
}
