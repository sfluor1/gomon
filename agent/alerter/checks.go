package alerter

import (
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"
	"gitlab.com/sfluor1/gomon/agent/db"
)

func (a Alerter) sendAlert(alr db.AlertRecord, now time.Time, ch chan Message) {
	// Send the alert to the agent broadcast channel
	ch <- alrtToMsg(alr)
	// Log the alert
	a.Lg.Log(alr)

	// Setup a notification for the hooks
	not := Notification{Timestamp: now.Unix(), Website: alr.URL}

	switch alr.Type {
	case db.DownAlert:
		{
			not.AvblAlerting = true
			not.Availability = alr.Availability
		}
	case db.AvbltyRecover:
		{
			not.Availability = alr.Availability
		}
	case db.RTAlert:
		{
			not.RTAlerting = true
			not.ResponseTime = alr.RespTime
		}
	case db.RTRecover:
		{
			not.ResponseTime = alr.RespTime
		}
	}

	a.notifyHooks(not)

}

// checkOneAvailability performs an Availability check on a single website
// Returns true if availability is alerting
func (a Alerter) checkOneAvailability(url string, database db.Database, now time.Time, ch chan Message) bool {
	availability := db.GetAvailability(database, url, a.TimeRange)

	wr, ok := database.Get(url)
	// No samples
	if !ok {
		return false
	}

	// New Alert
	if availability < config.AVBLTHRESHOLD && !wr.AvblAlerting {
		a.sendAlert(database.SetAvblAlert(url, true, availability), now, ch)
		return true
	}

	// Alert state ended
	if availability >= config.AVBLTHRESHOLD && wr.AvblAlerting {
		a.sendAlert(database.SetAvblAlert(url, false, availability), now, ch)
		return false
	}

	// No alert
	ch <- alrtToMsg(db.AlertRecord{})
	return false
}

// checkOneRespTime performs a ResponseTime check on a single website
// Returns true if response time is alerting
func (a Alerter) checkOneRespTime(url string, database db.Database, now time.Time, ch chan Message) bool {
	respTime, _, _ := db.GetRespTime(database, url, a.TimeRange)

	wr, ok := database.Get(url)
	// No samples
	if !ok {
		return false
	}

	// New Alert
	if respTime > a.RespTimeThresholds[url] && !wr.RTAlerting {
		a.sendAlert(database.SetRTAlert(url, true, respTime), now, ch)
		return true
	}

	// Alert state ended
	if respTime <= a.RespTimeThresholds[url] && wr.RTAlerting {
		a.sendAlert(database.SetRTAlert(url, false, respTime), now, ch)
		return false
	}

	// No alert
	ch <- alrtToMsg(db.AlertRecord{})
	return false
}

// Check performs lookups on the database by executing checkOneAvailability and checkOneRespTime on all the records
func (a Alerter) Check(database db.Database, ch chan Message) {

	ticker := time.NewTicker(a.refreshFreq)

	for {
		now := <-ticker.C
		for url := range database.GetAllRecords() {
			a.checkOneAvailability(url, database, now, ch)
			if _, ok := a.RespTimeThresholds[url]; ok {
				a.checkOneRespTime(url, database, now, ch)
			}
		}
	}

}
