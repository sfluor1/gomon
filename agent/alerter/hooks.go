package alerter

import (
	"bytes"
	"encoding/json"
	"net/http"
)

// Notification defines the JSON format for a hook alert notification
type Notification struct {
	Timestamp    int64   `json:"timestamp"`              // Timestamp of the Alert
	Website      string  `json:"website"`                // Concerned website
	Availability float64 `json:"availability,omitempty"` // Availability if it was an Availability alert
	RTAlerting   bool    `json:"rtAlerting,omitempty"`   // Whether it was a Response Time alert or not
	AvblAlerting bool    `json:"avblAlerting,omitempty"` // Whether it was an Availability alert or not
	ResponseTime float64 `json:"responseTime,omitempty"` // Response Time if it was a Response time alert
}

// notifyHooks send a post request to every hook with the given notification
func (a Alerter) notifyHooks(not Notification) {
	data, _ := json.Marshal(not)
	for _, hook := range a.Hooks {
		go a.notifyOneHook(hook, data)
	}
}

func (a Alerter) notifyOneHook(hook string, data []byte) {
	http.Post(hook, "JSON", bytes.NewBuffer(data))
}
