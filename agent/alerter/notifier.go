package alerter

import (
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"

	"gitlab.com/sfluor1/gomon/agent/db"
)

// Notifier represents the notifier that is responsible for sending metrics update to the clients via the agent broadcast channel
type Notifier struct {
	Section1 config.Section // The section 1 configuration from the configuration file
	Section2 config.Section // The section 2 configuration from the configuration file
}

// Message Types
// The PREVIOUSALERTS type is defined here even if the Notifier doesn't use it (it's only used by the agent directly)
const (
	SECTION1UPDATE uint8 = iota
	SECTION2UPDATE
	ALRTSECTIONUPDATE
	PREVIOUSALERTS
	CONFIGMSG
)

// Message is the standard format for a Message sent to the clients
type Message struct {
	Type      uint8            // Type of the message (can be of type section1/2/alrt update)
	Metrics   []db.Metrics     // Metrics payload if the message is about metrics
	Alerts    []db.AlertRecord // Alerts payload if the message is about alerts
	TimeStamp time.Time        // Timestamp of the message
}

// notifyMetrics gets metrics from the given database and push them as a Message to the given channel (the agent broadcast one)
func (n Notifier) notifyMetrics(now time.Time, duration time.Duration, database db.Database, ch chan Message, typ uint8) {
	metrics := db.GetAllMetrics(database, duration, now)
	data := []db.Metrics{}

	for _, m := range metrics {
		data = append(data, *m)
	}

	ch <- Message{Type: typ, Metrics: data, TimeStamp: time.Now()}
}

// notifyLoop runs notifyMetrics regularly
func (n Notifier) notifyLoop(interval, duration time.Duration, database db.Database, ch chan Message, typ uint8) {
	ticker := time.NewTicker(interval)

	for {
		now := <-ticker.C
		n.notifyMetrics(now, duration, database, ch, typ)
	}
}

// Notify runs two notify loops, one for each metrics section
func (n Notifier) Notify(database db.Database, ch chan Message) {
	go n.notifyLoop(
		time.Duration(n.Section1.RefreshFreq)*time.Second,
		time.Duration(n.Section1.TimeRange)*time.Second,
		database,
		ch,
		SECTION1UPDATE,
	)
	n.notifyLoop(
		time.Duration(n.Section2.RefreshFreq)*time.Second,
		time.Duration(n.Section2.TimeRange)*time.Second,
		database,
		ch,
		SECTION2UPDATE,
	)
}
