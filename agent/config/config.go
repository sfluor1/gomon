package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// AVBLTHRESHOLD Availability threshold for the alerting system (80%)
const AVBLTHRESHOLD = 0.8

// DB represents the database configuration
type DB struct {
	Database string `json:"database"` // The Database name
	Addr     string `json:"addr"`     // Address of the database host
	Username string `json:"username"` // Username
	Password string `json:"password"` // Password
}

// WebsiteWatcher represents the configuration for a given file in the json config
type WebsiteWatcher struct {
	URL               string  `json:"url"`               // The URL of the website (with the http/https prefix)
	CheckFrequency    uint    `json:"checkFrequency"`    // The CheckFrequency for this website in seconds
	RespTimeThreshold float64 `json:"respTimeThreshold"` // Threshold for the response time, if crossed an alert is declenched
}

// GomonConfig represents the whole json configuration
type GomonConfig struct {
	Watchers     []WebsiteWatcher `json:"watchers"` // List of website to watch
	Timeout      uint             `json:"timeout"`  // The timeout to consider for the check requests
	Hooks        []string         `json:"hooks"`    // List of urls to call in case of alert
	Section1     Section          `json:"section1"`
	Section2     Section          `json:"section2"`
	AlertSection Section          `json:"alertSection"`
	DB           DB               `json:"db"` // Database configuration
}

// Section represents the configuration for a section of the user interface
type Section struct {
	TimeRange   uint `json:"timeRange"`   // Given time range to analyze for a Section
	RefreshFreq uint `json:"refreshFreq"` // Refresh frequency
}

// Parse parses a given json file and returns the configuration formatted in the GomonConfig struct format
func Parse(path string) *GomonConfig {
	f, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "An error occurred while reading the config file: %v\n", err)
		os.Exit(1)
	}

	conf := &GomonConfig{}

	if err := json.Unmarshal(f, conf); err != nil {
		fmt.Fprintf(os.Stderr, "An error occurred while parsing the config file: %v\n", err)
		os.Exit(1)
	}

	return conf
}
