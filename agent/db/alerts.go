package db

import "time"

// Alert types
const (
	AvbltyRecover uint = iota // Availability Recover
	DownAlert                 // Down alert
	RTAlert                   // Response Time alert
	RTRecover                 // Response Time recover
)

// AlertRecord defines how an alert is stored in the database
type AlertRecord struct {
	TimeStamp    time.Time // Timestamp of the alert
	URL          string    // URL of the concerned website
	Type         uint      // Type of the alert
	Availability float64   // Availability of the website at the moment of the alert
	RespTime     float64   // Response time of the website at the moment of the alert
}
