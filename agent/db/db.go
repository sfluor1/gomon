package db

import (
	"fmt"
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"
)

// DataPoint stores a DataPoint metric for a given URL
type DataPoint struct {
	URL          string    // The URL of the website
	StatusCode   int       // The status code received when doing the check request
	ResponseTime float64   // The response time of the request
	Up           bool      // If the website responded correctly
	ErrorMessage string    // The possible error message returned by the check request
	TimeStamp    time.Time // Moment where the request was sent
}

// WebRecord stores all the data for a given website
type WebRecord struct {
	AvblAlerting bool        // If the website availability is critical
	RTAlerting   bool        // If the website response time is critical
	Points       []DataPoint // The list of the website metrics
}

// Database is an interface modeling the database
type Database interface {
	AddRecord(point DataPoint)
	Get(url string) (WebRecord, bool)
	SetRTAlert(url string, state bool, respTime float64) AlertRecord       // Set Response Time Alert
	SetAvblAlert(url string, state bool, availability float64) AlertRecord // Set Availability Alert
	GetRecord(url string, since time.Duration, now time.Time) ([]DataPoint, bool)
	GetAllRecords() map[string]*WebRecord
	GetAllAlerts() []AlertRecord
	Close() // Close the connection to the database
}

// HandleDataFlow simply listen on a channel and adds every datapoint received to the database
func HandleDataFlow(d Database, c chan (DataPoint)) {
	for {
		point := <-c
		d.AddRecord(point)
	}
}

// New creates a new database instance
// If a valid configuration is provided, it will try to connect to a MongoDB database
// Otherwise it will use an In memory database
func New(config config.DB) Database {
	if config.Addr != "" && config.Database != "" {
		fmt.Println("Using mongodb backend for the database")
		return newMongoDB(config)
	}

	fmt.Println("Using in-memory database")
	return newInMemDB()
}
