package db

import (
	"fmt"
	"testing"
	"time"
)

const nRecords = 100
const nAlerts = 4 * nRecords

func testDB(db Database, t *testing.T) {
	now := time.Now()

	for i := 1; i <= nRecords; i++ {
		db.AddRecord(DataPoint{
			URL:       fmt.Sprintf("%v", i),
			TimeStamp: now,
		})

		db.AddRecord(DataPoint{
			URL:       fmt.Sprintf("%v", i),
			TimeStamp: now.Add(-24 * time.Hour),
		})
	}

	testDBGetRecord(db, now, t)
	testDBSetAlert(db, now, t)
	testDBGetAllRecords(db, t)
	testDBGetAllAlerts(db, t)
}

func testDBGetRecord(db Database, now time.Time, t *testing.T) {

	for i := 1; i <= nRecords; i++ {
		d, ok := db.GetRecord(fmt.Sprintf("%v", i), time.Hour, now)
		if !ok {
			t.Errorf("Sample %v should be in the database", i)
		}

		if len(d) != 1 {
			t.Errorf("More than one DataPoint found for key %v, (%v)", i, d)
		}
	}
}

func testDBSetAlert(db Database, now time.Time, t *testing.T) {

	for i := 1; i <= nRecords; i++ {
		url := fmt.Sprintf("%v", i)
		db.SetRTAlert(url, true, 0.5)
		db.SetAvblAlert(url, true, 0.6)
		rec, ok := db.Get(url)
		if !ok || !rec.AvblAlerting || !rec.RTAlerting {
			t.Errorf("Test on %v failed: both states should be alerting but are: (%t, %t)", i, rec.AvblAlerting, rec.RTAlerting)
		}
	}

	for i := 1; i <= nRecords; i++ {
		url := fmt.Sprintf("%v", i)
		db.SetRTAlert(url, false, 0.2)
		db.SetAvblAlert(url, false, 0.9)
		rec, ok := db.Get(url)
		if !ok || rec.AvblAlerting || rec.RTAlerting {
			t.Errorf("Test on %v failed: both states should be not-alerting but are: (%t, %t)", i, rec.AvblAlerting, rec.RTAlerting)
		}
	}
}

func testDBGetAllRecords(db Database, t *testing.T) {
	records := db.GetAllRecords()

	if len(records) != nRecords {
		t.Errorf("Records are missing, there should be %d records but there are %d", nRecords, len(records))
	}

	for url, data := range records {
		if len(data.Points) != 2 {
			t.Errorf("There should be 2 data points for key %v, (%v)", url, data)
		}
	}
}

func testDBGetAllAlerts(db Database, t *testing.T) {
	alerts := db.GetAllAlerts()

	if len(alerts) != nAlerts {
		t.Errorf("Alerts are missing, there should be %d alerts but there are %d", nAlerts, len(alerts))
	}

	for _, alert := range alerts {
		if alert.Type != AvbltyRecover && alert.Type != DownAlert && alert.Type != RTRecover && alert.Type != RTAlert {
			t.Errorf("Wrong alert type: %v", alert.Type)
		}
	}
}
