package db

import (
	"sync"
	"time"
)

// InMemDB is a simply hashmap modeling the database, the keys are the URL of the websites and the values their webrecords
type InMemDB struct {
	records map[string]*WebRecord
	alerts  []AlertRecord
	mutex   *sync.Mutex
}

func newInMemDB() *InMemDB {
	return &InMemDB{
		map[string]*WebRecord{}, []AlertRecord{}, &sync.Mutex{},
	}
}

func (d *InMemDB) Close() {}

// AddRecord adds a DataPoint to the database, if the website is not listed in the database, it's created
func (d *InMemDB) AddRecord(point DataPoint) {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	_, ok := d.records[point.URL]

	if !ok {
		d.records[point.URL] = &WebRecord{Points: []DataPoint{}}
	}

	d.records[point.URL].Points = append(d.records[point.URL].Points, point)
}

// GetRecord retrieves all the DataPoints for a given website and for a given time range
// An error is returned if the website is not found in the database
func (d *InMemDB) GetRecord(url string, since time.Duration, now time.Time) ([]DataPoint, bool) {
	wr, ok := d.Get(url)

	if !ok {
		return []DataPoint{}, ok
	}

	length := len(wr.Points)
	res := []DataPoint{}
	for i := range wr.Points {
		if p := wr.Points[length-i-1]; now.Sub(p.TimeStamp) <= since {
			res = append(res, p)
		}
	}

	return res, ok
}

// GetAllRecords returns all the records of the database
func (d *InMemDB) GetAllRecords() map[string]*WebRecord {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	res := map[string]*WebRecord{}
	for k, v := range d.records {
		res[k] = v
	}
	return res
}

func (d *InMemDB) Get(url string) (WebRecord, bool) {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	value, ok := d.records[url]

	if !ok {
		return WebRecord{}, ok
	}
	return *value, ok
}

func (d *InMemDB) SetRTAlert(url string, state bool, respTime float64) AlertRecord {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	_, ok := d.records[url]
	if !ok {
		d.records[url] = &WebRecord{}
	}
	d.records[url].RTAlerting = state
	rec := AlertRecord{TimeStamp: time.Now(), URL: url, Type: RTRecover, RespTime: respTime}

	if state {
		rec.Type = RTAlert
	}

	d.alerts = append(d.alerts, rec)
	return rec
}

func (d *InMemDB) SetAvblAlert(url string, state bool, availability float64) AlertRecord {
	d.mutex.Lock()
	defer d.mutex.Unlock()
	_, ok := d.records[url]
	if !ok {
		d.records[url] = &WebRecord{}
	}

	d.records[url].AvblAlerting = state
	rec := AlertRecord{TimeStamp: time.Now(), URL: url, Type: AvbltyRecover, Availability: availability}

	if state {
		rec.Type = DownAlert
	}

	d.alerts = append(d.alerts, rec)
	return rec
}

func (d InMemDB) GetAllAlerts() []AlertRecord { return d.alerts }
