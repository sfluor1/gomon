package db

import (
	"testing"

	"gitlab.com/sfluor1/gomon/agent/config"
)

func TestInMemDB(t *testing.T) {
	db := New(config.DB{})
	testDB(db, t)
}
