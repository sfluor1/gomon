package db

import (
	"sort"
	"time"
)

// Metrics store all the metrics for a given website for a given time interval
type Metrics struct {
	URL             string // The website URL
	MinResponseTime float64
	MaxResponseTime float64
	AvgResponseTime float64
	Availability    float64
	StatusCodes     map[int]int
}

// GetRespTime return the Average, Min, Max Response Time of a given website for a given interval
func GetRespTime(d Database, url string, since time.Duration) (float64, float64, float64) {
	records, ok := d.GetRecord(url, since, time.Now())

	if !ok || len(records) == 0 {
		return 0, 0, 0
	}

	var avg, min, max, count float64

	for _, rec := range records {
		// Count only when it was up
		if rec.Up {
			avg += rec.ResponseTime
			count++

			if min == 0 || rec.ResponseTime < min {
				min = rec.ResponseTime
			}
			if rec.ResponseTime > max {
				max = rec.ResponseTime
			}
		}

	}

	if count > 0 {
		avg /= count
	}
	return avg, min, max
}

// GetAvailability return the availability percentage of a given website for a given duration since now
// If the website is not listed it returns 0 and if there are not records yet it returns 1
func GetAvailability(d Database, url string, since time.Duration) float64 {
	records, ok := d.GetRecord(url, since, time.Now())

	if !ok || len(records) == 0 {
		return 1
	}

	var availability float64
	for _, point := range records {
		if point.Up {
			availability++
		}
	}

	return availability / float64(len(records))
}

// GetStatusCodes return the status codes received by a given website for a given duration since now
// If the website is not listed it returns an empty dict
func GetStatusCodes(d Database, url string, since time.Duration) map[int]int {
	records, ok := d.GetRecord(url, since, time.Now())

	if !ok || len(records) == 0 {
		return map[int]int{}
	}

	codes := map[int]int{}
	for _, point := range records {
		if point.StatusCode != 0 {
			val, ok := codes[point.StatusCode]
			if !ok {
				codes[point.StatusCode] = 1
			} else {
				codes[point.StatusCode] = val + 1
			}
		}
	}

	return codes
}

// GetMetrics return the Metrics of a given website for a given interval
// Not efficient since it loops 3 times but its more readable
func GetMetrics(d Database, url string, since time.Duration, now time.Time) *Metrics {
	records, ok := d.GetRecord(url, since, time.Now())

	if !ok || len(records) == 0 {
		return &Metrics{}
	}

	metrics := &Metrics{URL: url}
	metrics.Availability = GetAvailability(d, url, since)
	metrics.StatusCodes = GetStatusCodes(d, url, since)
	avg, min, max := GetRespTime(d, url, since)
	metrics.AvgResponseTime = avg
	metrics.MaxResponseTime = max
	metrics.MinResponseTime = min

	return metrics
}

// GetAllMetrics returns the Metrics for all the websites in the database and sort them by decreasing average response time
func GetAllMetrics(d Database, since time.Duration, now time.Time) []*Metrics {
	res := []*Metrics{}

	for url := range d.GetAllRecords() {
		m := GetMetrics(d, url, since, now)
		res = append(res, m)
	}
	sort.Sort(byRespTime(res))

	return res
}

// type to sort metrics by decreasing response time
type byRespTime []*Metrics

func (m byRespTime) Len() int {
	return len(m)
}
func (m byRespTime) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}
func (m byRespTime) Less(i, j int) bool {
	return m[i].AvgResponseTime > m[j].AvgResponseTime
}
