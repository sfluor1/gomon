package db

import (
	"math"
	"reflect"
	"testing"
	"time"
)

// float equal
func fleq(a, b float64) bool {
	tol := 0.00001
	return math.Abs(b-a) < tol
}

func TestGetAvailability(t *testing.T) {
	now := time.Now()

	tt := map[string]struct {
		wr       *WebRecord
		expected float64
		desc     string
	}{
		"1": {&WebRecord{}, 1, "empty record"},
		"2": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now},
				DataPoint{Up: true, TimeStamp: now},
				DataPoint{Up: true, TimeStamp: now},
				DataPoint{TimeStamp: now},
				DataPoint{TimeStamp: now},
			}},
			0.6,
			"3/5 of availability",
		},
		"3": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now},
				DataPoint{TimeStamp: now},
			}},
			0.5,
			"1/2 of availability",
		},
		"4": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{TimeStamp: now},
			}},
			0,
			"5/6 of availability but over the last hour only (0 over the last 2 minutes)",
		},
		"5": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
			}},
			1,
			"only old records",
		},
	}

	for key, tc := range tt {
		database := newInMemDB()
		for _, p := range tc.wr.Points {
			p.URL = key
			database.AddRecord(p)
		}
		if res := GetAvailability(database, key, time.Minute); tc.expected != res {
			t.Errorf("Test n°%v (%s) failed, expected %f but got %f", key, tc.desc, tc.expected, res)
		}
	}
}

func TestGetRespTime(t *testing.T) {
	now := time.Now()

	tt := map[string]struct {
		wr          *WebRecord
		expectedAvg float64
		expectedMin float64
		expectedMax float64
		desc        string
	}{
		"1": {&WebRecord{}, 0, 0, 0, "empty record"},
		"2": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, ResponseTime: 1, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.8, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.2, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.7, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.3, TimeStamp: now},
			}},
			0.6, 0.2, 1,
			"",
		},
		"3": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, ResponseTime: 0.3, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.4, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.5, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.6, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.4, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.2, TimeStamp: now},
			}},
			0.3, 0.2, 0.4,
			"",
		},
		"4": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.2, TimeStamp: now},
			}},
			0.2, 0.2, 0.2,
			"",
		},
		"5": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
			}},
			0, 0, 0,
			"only old records",
		},
	}

	for key, tc := range tt {
		database := newInMemDB()
		for _, p := range tc.wr.Points {
			p.URL = key
			database.AddRecord(p)
		}
		if avg, min, max := GetRespTime(database, key, time.Minute); !fleq(tc.expectedAvg, avg) || !fleq(tc.expectedMin, min) || !fleq(tc.expectedMax, max) {
			t.Errorf(
				"Test n°%v (%s) failed, expected (%f, %f, %f) but got (%f, %f, %f)",
				key,
				tc.desc,
				tc.expectedMin,
				tc.expectedAvg,
				tc.expectedMax,
				min, avg, max,
			)
		}
	}
}

func TestGetStatusCodes(t *testing.T) {
	now := time.Now()

	tt := map[string]struct {
		wr            *WebRecord
		expectedCodes map[int]int
	}{
		"1": {&WebRecord{}, map[int]int{}},
		"2": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now},
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now},
				DataPoint{Up: true, StatusCode: 302, TimeStamp: now},
				DataPoint{Up: true, StatusCode: 302, TimeStamp: now},
				DataPoint{StatusCode: 404, TimeStamp: now},
			}},
			map[int]int{100: 2, 302: 2, 404: 1},
		},
		"3": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now},
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now},
			}},
			map[int]int{100: 2},
		},
		"4": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 200, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 300, TimeStamp: now.Add(-time.Hour)},
				DataPoint{StatusCode: 400, TimeStamp: now.Add(-time.Hour)},
			}},
			map[int]int{},
		},
		"5": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 200, TimeStamp: now},
				DataPoint{Up: true, StatusCode: 300, TimeStamp: now.Add(-time.Hour)},
				DataPoint{StatusCode: 400, TimeStamp: now},
				DataPoint{Up: true, StatusCode: 100, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 200, TimeStamp: now},
			}},
			map[int]int{200: 2, 400: 1},
		},
	}

	for key, tc := range tt {
		database := newInMemDB()
		for _, p := range tc.wr.Points {
			p.URL = key
			database.AddRecord(p)
		}
		codes := GetStatusCodes(database, key, 5*time.Minute)
		if !reflect.DeepEqual(codes, tc.expectedCodes) {
			t.Errorf("Test n°%s failed, expected %v but got %v", key, tc.expectedCodes, codes)
		}
	}
}

func TestGetMetrics(t *testing.T) {
	now := time.Now()

	tt := map[string]struct {
		wr              *WebRecord
		expectedMetrics Metrics
	}{
		"1": {&WebRecord{}, Metrics{}},
		"2": {
			&WebRecord{Points: []DataPoint{
				DataPoint{ResponseTime: 1, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.8, TimeStamp: now},
				DataPoint{StatusCode: 404, ResponseTime: 0.2, TimeStamp: now},
				DataPoint{ResponseTime: 0.7, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.3, TimeStamp: now},
			}},
			Metrics{"2", 0.3, 0.8, 0.55, 0.4, map[int]int{404: 1}},
		},
		"3": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, ResponseTime: 0.3, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.4, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.5, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.6, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, ResponseTime: 0.4, TimeStamp: now},
				DataPoint{Up: true, ResponseTime: 0.3, TimeStamp: now},
			}},
			Metrics{"3", 0.3, 0.4, 0.35, 1, map[int]int{}},
		},
		"4": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, StatusCode: 300, ResponseTime: 0.2, TimeStamp: now},
			}},
			Metrics{"4", 0.2, 0.2, 0.2, 1, map[int]int{300: 1}},
		},
		"5": {
			&WebRecord{Points: []DataPoint{
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
				DataPoint{Up: true, TimeStamp: now.Add(-time.Hour)},
			}},
			Metrics{},
		},
	}

	for key, tc := range tt {
		database := newInMemDB()
		for _, p := range tc.wr.Points {
			p.URL = key
			database.AddRecord(p)
		}
		metrics := GetMetrics(database, key, 5*time.Minute, now)

		if !reflect.DeepEqual(*metrics, tc.expectedMetrics) {
			t.Errorf("Test n°%s failed, expected %v but got %v", key, tc.expectedMetrics, metrics)
		}
	}
}
