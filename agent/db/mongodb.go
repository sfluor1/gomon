package db

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// MongoDB defines a mongodb connection
type MongoDB struct {
	metrics *mgo.Collection // The metrics collection
	alerts  *mgo.Collection // The alerts collection
	session *mgo.Session    // The mongodb session
}

// MongoWebRecord is an element of the metrics collection in the mongodb database
type MongoWebRecord struct {
	AvblAlerting bool        // If the website availability is critical
	RTAlerting   bool        // If the website response time is critical
	Points       []DataPoint `bson:"points"` // The list of the website metrics
	URL          string      `bson:"url"`
}

// newMongoDB instantiates a new mongodb connection
func newMongoDB(config config.DB) *MongoDB {
	session, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    []string{config.Addr},
		Timeout:  10 * time.Second,
		Database: config.Database,
		Username: config.Username,
		Password: config.Password,
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't create a mongodb session: %s", err)
	}

	session.SetMode(mgo.Monotonic, true)

	// Index the database metrics collection on the url key
	index := mgo.Index{
		Key:    []string{"url"},
		Unique: true,
	}

	met := session.DB(config.Database).C("metrics")
	alr := session.DB(config.Database).C("alerts")

	err = met.EnsureIndex(index)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't init indexing on the mongodb database: %s", err)
	}

	return &MongoDB{met, alr, session}
}

func (d *MongoDB) Close() {
	d.session.Clone()
}

func (d *MongoDB) AddRecord(point DataPoint) {
	_, ok := d.Get(point.URL)

	if !ok {
		err := d.metrics.Insert(&MongoWebRecord{Points: []DataPoint{point}, URL: point.URL})
		if err != nil {
			fmt.Fprintf(os.Stderr, "Couldn't add record %v: %s\n", point, err)
		}
	} else {
		err := d.metrics.Update(bson.M{"url": point.URL}, bson.M{"$push": bson.M{"points": point}})
		if err != nil {
			fmt.Fprintf(os.Stderr, "Couldn't add record %v: %s\n", point, err)
		}
	}
}

func (d *MongoDB) Get(url string) (WebRecord, bool) {
	result := MongoWebRecord{}

	err := d.metrics.Find(bson.M{"url": url}).One(&result)

	if err != nil {
		return WebRecord{}, false
	}

	return WebRecord{AvblAlerting: result.AvblAlerting, RTAlerting: result.RTAlerting, Points: result.Points}, true
}
func (d *MongoDB) SetRTAlert(url string, state bool, respTime float64) AlertRecord {
	err := d.metrics.Update(bson.M{"url": url}, bson.M{"$set": bson.M{"rtalerting": state}})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't set response time alert for %s: %s\n", url, err)
	}
	rec := AlertRecord{TimeStamp: time.Now(), URL: url, Type: RTRecover, RespTime: respTime}

	if state {
		rec.Type = RTAlert
	}

	err = d.alerts.Insert(rec)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't add alert %v: %s\n", rec, err)
	}

	return rec
}

func (d *MongoDB) SetAvblAlert(url string, state bool, availability float64) AlertRecord {
	err := d.metrics.Update(bson.M{"url": url}, bson.M{"$set": bson.M{"avblalerting": state}})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't set availability alert for %s: %s\n", url, err)
	}
	rec := AlertRecord{TimeStamp: time.Now(), URL: url, Type: AvbltyRecover, Availability: availability}

	if state {
		rec.Type = DownAlert
	}

	err = d.alerts.Insert(rec)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't add alert %v: %s\n", rec, err)
	}

	return rec
}

func (d *MongoDB) GetRecord(url string, since time.Duration, now time.Time) ([]DataPoint, bool) {
	var res map[string][]DataPoint

	// Mongo db query
	// db.metrics.aggregate(
	// 	[
	// 		{"$match": {"url": "$url"}},
	// 		{"$unwind": "$points"},
	// 		{"$match": {
	// 			"$and": [
	// 				{"points.timestamp": {"$gt": ISODate("2018-04-05T09:32:46.755Z")}},
	// 				{"points.timestamp": {"$lt": ISODate("2018-04-05T09:43:46.755Z")}}
	// 				]
	// 				}
	// 		},
	// 		{"$group": {
	// 			"_id" : "$url",
	// 			"points": {"$push": "$points"}
	// 			}
	// 		}
	// 	]
	// )

	pipe := d.metrics.Pipe(
		[]bson.M{
			bson.M{"$match": bson.M{"url": url}},
			bson.M{"$unwind": "$points"},
			bson.M{"$match": bson.M{"$and": []bson.M{
				bson.M{"points.timestamp": bson.M{"$gte": now.Add(-since)}},
				bson.M{"points.timestamp": bson.M{"$lte": now}},
			}}},
			bson.M{"$group": bson.M{
				"_id":    "$url",
				"points": bson.M{"$push": "$points"},
			}},
		},
	)

	pipe.One(&res)
	if res == nil {
		return []DataPoint{}, false
	}

	v, ok := res["points"]

	return v, ok
}
func (d *MongoDB) GetAllRecords() map[string]*WebRecord {
	res := map[string]*WebRecord{}

	var temp []*MongoWebRecord

	err := d.metrics.Find(nil).All(&temp)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't get all the records: %s\n", err)
	}

	for _, mwr := range temp {
		res[mwr.URL] = &WebRecord{mwr.AvblAlerting, mwr.RTAlerting, mwr.Points}
	}

	return res
}

func (d *MongoDB) GetAllAlerts() []AlertRecord {
	var res []AlertRecord
	err := d.alerts.Find(nil).All(&res)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Couldn't get all the alerts: %s\n", err)
	}
	return res
}
