package db

import (
	"testing"

	"gitlab.com/sfluor1/gomon/agent/config"
)

func TestMongoDB(t *testing.T) {
	db := New(config.DB{Database: "testmongodb", Addr: "mongo:27017"})
	err := db.(*MongoDB).session.DB("testmongodb").DropDatabase()
	if err != nil {
		panic(err)
	}

	testDB(db, t)
}
