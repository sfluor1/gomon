package logger

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/sfluor1/gomon/agent/db"
)

const TimeFormat = "2006-01-02 15:04:05"

// Logger struct
type Logger struct {
	Out io.Writer // The writer interface to which the Logger will write
}

// New simply returns a logger that can be used for the alerting sytem
// If a file is provided the logger will log both to the first given writer and the file
func New(w io.Writer, f *os.File) *Logger {
	writers := []io.Writer{w}

	if f != nil {
		writers = append(writers, f)
	}

	return &Logger{io.MultiWriter(writers...)}
}

// Log simply logs a message on the Logger writer
func (l Logger) Log(alrt db.AlertRecord) {
	fmt.Fprint(l.Out, formatAlertLog(alrt))
}

func formatAlertLog(alrt db.AlertRecord) string {
	switch alrt.Type {
	case db.DownAlert:
		{
			return fmt.Sprintf("%s - %s - CRITICAL - Availability is critical: %.5f %%\n", alrt.TimeStamp.Format(TimeFormat), alrt.URL, alrt.Availability)
		}
	case db.AvbltyRecover:
		{
			return fmt.Sprintf("%s - %s - INFO - Availability recovered: %.5f %%\n", alrt.TimeStamp.Format(TimeFormat), alrt.URL, alrt.Availability)
		}
	case db.RTAlert:
		{
			return fmt.Sprintf("%s - %s - WARNING - Response time is critical: %.5f s\n", alrt.TimeStamp.Format(TimeFormat), alrt.URL, alrt.RespTime)
		}
	case db.RTRecover:
		{
			return fmt.Sprintf("%s - %s - INFO - Response time recovered: %.5f s\n", alrt.TimeStamp.Format(TimeFormat), alrt.URL, alrt.RespTime)
		}
	}
	return ""
}
