package watcher

import (
	"net/http"
	"sync"
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"
	"gitlab.com/sfluor1/gomon/agent/db"
)

// Watcher represents the struct responsible for checking the website states
type Watcher struct {
	client *http.Client        // The HTTP client used to make the requests
	Wg     *sync.WaitGroup     // A waitgroup to keep the Watcher watching all the websites
	config config.GomonConfig  // The configuration file as a GomonConfig struct
	Data   chan (db.DataPoint) // A channel where metrics are piped when received for further use
}

// New takes a configuration struct and returns a Watcher reference
func New(conf config.GomonConfig) *Watcher {

	return &Watcher{
		&http.Client{
			Timeout:   time.Duration(conf.Timeout) * time.Second,
			Transport: &http.Transport{MaxIdleConnsPerHost: 100},
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		},
		&sync.WaitGroup{},
		conf,
		make(chan (db.DataPoint)),
	}
}

// collect simply takes an url and return the metrics for this website by doing a get request
func (w Watcher) collect(url string) db.DataPoint {
	beginning := time.Now()
	res, err := w.client.Get(url)
	responseTime := time.Since(beginning).Seconds()

	dp := db.DataPoint{URL: url, Up: false, ResponseTime: responseTime, TimeStamp: time.Now()}

	if err != nil {
		dp.ErrorMessage = "Couldn't reach the website"
	} else {
		dp.StatusCode = res.StatusCode
		dp.Up = int(res.StatusCode/100) <= 3
	}

	return dp
}

// watchOne takes care of watching a given website at a given interval and sends the collected data to the Data channel
func (w Watcher) watchOne(webWatcher config.WebsiteWatcher) {
	defer w.Wg.Done()
	ticker := time.NewTicker(time.Duration(webWatcher.CheckFrequency) * time.Second)
	for {
		_ = <-ticker.C
		w.Data <- w.collect(webWatcher.URL)
	}

}

// Watch spawns multiple go routines to watch every website listed in the configuration
func (w Watcher) Watch() {

	for _, el := range w.config.Watchers {
		w.Wg.Add(1)
		go w.watchOne(el)
	}

	w.Wg.Wait()
}
