package watcher

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/sfluor1/gomon/agent/config"
)

func TestCollect(t *testing.T) {
	tt := []struct {
		srv           *httptest.Server
		expectedState bool
		descr         string
	}{
		{
			httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "application/json")
				fmt.Fprintln(w, `{"this is a test server"}`)
			})),
			true,
			"Up server",
		},
		{
			httptest.NewServer(nil),
			false,
			"Down server",
		},
		{
			httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNotFound)
				fmt.Fprintln(w, `{"this is a 404 server"}`)
			})),
			false,
			"404 server",
		},
	}

	watcher := New(config.GomonConfig{Timeout: 10})

	for _, tc := range tt {
		dp := watcher.collect(tc.srv.URL)

		if dp.Up != tc.expectedState {
			t.Errorf("Test (%s) failed, test server should be labeled as %t but got %t", tc.descr, tc.expectedState, dp.Up)
		}

		if dp.ResponseTime <= 0 {
			t.Errorf("Test (%s) failed, response time shouldn't be negative", tc.descr)
		}

		if dp.URL != tc.srv.URL {
			t.Errorf("Test (%s) failed, the url registered is %s, expected %s", tc.descr, dp.URL, tc.srv.URL)
		}
	}
}
