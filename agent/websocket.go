package agent

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/sfluor1/gomon/agent/db"

	"github.com/gorilla/websocket"
	"gitlab.com/sfluor1/gomon/agent/alerter"
)

// custom type for a connection
type connection struct {
	ws   *websocket.Conn      // Websocket connection
	send chan alerter.Message // A buffered channel of all the messages that needs to be sent to the client
}

// server struct to manage the connections
type server struct {
	broadcast   chan alerter.Message // Broadcast channel for the broadcast messages
	register    chan *connection     // Register queue
	unregister  chan *connection     // Unregister queue
	connections map[*connection]bool // List of all the connections
	cache       cache                // A cache for the last computed metrics and the previous alerts
}

type cache struct {
	alerts   []db.AlertRecord
	section1 []db.Metrics
	section2 []db.Metrics
}

func (a *Agent) wsHandler(w http.ResponseWriter, r *http.Request) {
	// If restrict mode is activated check origin
	if a.restrict && r.Header.Get("Origin") != "http://"+r.Host {
		http.Error(w, "Origin not allowed", 403)
		return
	}

	c, err := websocket.Upgrade(w, r, w.Header(), 1024, 1024)
	if err != nil {
		http.Error(w, "Could not open websocket connection", http.StatusBadRequest)
	}

	// Send the config to the client
	c.WriteJSON(a.config)

	// Send the previous alerts
	c.WriteJSON(alerter.Message{Type: alerter.PREVIOUSALERTS, Alerts: a.server.cache.alerts, TimeStamp: time.Now()})

	// Send the cached metrics
	c.WriteJSON(alerter.Message{Type: alerter.SECTION1UPDATE, Metrics: a.server.cache.section1, TimeStamp: time.Now()})
	c.WriteJSON(alerter.Message{Type: alerter.SECTION2UPDATE, Metrics: a.server.cache.section2, TimeStamp: time.Now()})

	// Create the connection with a buffered message channel of length 2048
	con := &connection{c, make(chan alerter.Message, 2048)}
	a.server.register <- con

	// Run a go routine for this specific client to send him messages
	go a.notify(con)
}

// notify handles a specific connection by listening on the connection channel and sending messages when it's needed
func (a *Agent) notify(con *connection) {
	for {
		msg := <-con.send

		if err := con.ws.WriteJSON(msg); err != nil {
			a.server.unregister <- con
			return
		}
	}
}

func (a *Agent) run() {
	sigs := make(chan os.Signal, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		// Registration
		case con := <-a.server.register:
			a.server.connections[con] = true
			// Unregistration
		case con := <-a.server.unregister:
			if _, ok := a.server.connections[con]; ok {
				delete(a.server.connections, con)
				close(con.send)
			}
			// Broadcast
		case msg := <-a.server.broadcast:
			a.server.cacheMessage(msg)
			for con := range a.server.connections {
				select {
				case con.send <- msg:
				default:
					delete(a.server.connections, con)
					close(con.send)
					con.ws.Close()
				}
			}
			// Syscall
		case <-sigs:
			for con := range a.server.connections {
				delete(a.server.connections, con)
				close(con.send)
				con.ws.Close()
			}
			a.database.Close()
			fmt.Printf("Exiting...")
			os.Exit(0)
		}

	}
}

func (srv *server) cacheMessage(msg alerter.Message) {
	switch msg.Type {
	case alerter.SECTION1UPDATE:
		{
			srv.cache.section1 = msg.Metrics
		}
	case alerter.SECTION2UPDATE:
		{
			srv.cache.section2 = msg.Metrics
		}
	case alerter.ALRTSECTIONUPDATE:
		{
			srv.cache.alerts = append(srv.cache.alerts, msg.Alerts...)
		}
	}
}
