package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/sfluor1/gomon/agent"
	"gitlab.com/sfluor1/gomon/agent/config"
)

// Flags for agent command
var configFilePath string
var logFilePath string
var port uint
var restricted bool

// Flags for ping command
var pingAddr string

func init() {
	backCmd.AddCommand(pingCmd)
	rootCmd.AddCommand(backCmd)

	pingCmd.Flags().StringVarP(&pingAddr, "addr", "a", "http://localhost:4444", "Address of the gomon agent")
	// Flags
	backCmd.Flags().StringVarP(&configFilePath, "config", "c", "", "Path to the gomon configuration file")
	backCmd.MarkFlagRequired("config")

	backCmd.Flags().StringVarP(&logFilePath, "log", "l", "", "Path to a log file if you want to enable alerts logging")
	backCmd.Flags().UintVarP(&port, "port", "p", 4444, "Port on which to listen for websocket connections")
	restricted = *backCmd.Flags().BoolP("restrict", "r", false, "If restrict mode should be enabled (refuse all websocket connections without a correct Origin header)")
}

var backCmd = &cobra.Command{
	Use:   "agent",
	Short: "Starts the gomon agent",
	Long:  `The gomon agent is responsible for website metrics polling, alerting system and database handling`,
	Run: func(cmd *cobra.Command, args []string) {

		var logFile *os.File

		if configFilePath == "" {
			fmt.Fprintln(os.Stderr, "You must provide a configuration file with the --config option")
			os.Exit(1)
		}

		if logFilePath != "" {
			var err error
			logFile, err = os.Create(logFilePath)
			if err != nil {
				fmt.Fprintf(os.Stderr, "An error occurred while opening the log file: %v\n", err)
				os.Exit(1)
			}
		}

		config := config.Parse(configFilePath)
		agent := agent.New(*config, logFile, restricted)
		agent.Start(port)
	},
}

var pingCmd = &cobra.Command{
	Use:   "ping",
	Short: "Pings the gomon agent",
	Long:  `Send a get request to the gomon agent to see if it's up and return its uptime`,
	Run: func(cmd *cobra.Command, args []string) {

		resp, err := http.Get(fmt.Sprintf("%s/ping", pingAddr))

		if err != nil {
			fmt.Fprintf(os.Stderr, "An error occurred: %s\n", err)
			os.Exit(1)
		}

		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Fprintf(os.Stderr, "An error occurred reading the request body: %s\n", err)
			os.Exit(1)
		}

		fmt.Print(string(data))
	},
}
