package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/sfluor1/gomon/dashboard"
)

var addr string

// var silent bool

func init() {
	rootCmd.AddCommand(dashboardCmd)
	// Flags
	dashboardCmd.Flags().StringVarP(&addr, "addr", "a", "localhost:4444", "Address of the agent to connect to")
}

var dashboardCmd = &cobra.Command{
	Use:   "dashboard",
	Short: "Starts the gomon cli-dashboard",
	Long:  `The gomon cli dashboard uses websockets to print information about websites like metrics and alerts`,
	Run: func(cmd *cobra.Command, args []string) {

		dash := dashboard.New(addr)

		dash.Start()
	},
}
