package cmd

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/spf13/cobra"
)

var mockPort uint

func init() {
	rootCmd.AddCommand(mockCmd)
	// Flags
	mockPort = *mockCmd.Flags().UintP("port", "p", 8888, "Port that the mock server needs to listen on")
}

var mockCmd = &cobra.Command{
	Use:   "mock",
	Short: "Starts a mock server that returns random Status codes",
	Long:  `The gomon cli mock server can be used to manually test the alerting logic of the gomon agent`,
	Run: func(cmd *cobra.Command, args []string) {
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			s1 := rand.NewSource(time.Now().UnixNano())
			r1 := rand.New(s1)
			// 300/375 = 0.8 we have 20% of probability of being alerting and 80% of being UP
			w.WriteHeader(100 + r1.Intn(375))
			fmt.Fprintf(w, "Pong")
		})
		fmt.Print("Starting the mock server...")
		panic(http.ListenAndServe(fmt.Sprintf(":%d", mockPort), nil))
	},
}
