package dashboard

// Cyan background
func cyanBg(str string) string {
	return "\033[36;7m" + str + "\033[0m"
}

func green(str string) string {
	return "\033[32m" + str + "\033[0m"
}

func red(str string) string {
	return "\033[31m" + str + "\033[0m"
}

func yellow(str string) string {
	return "\033[33m" + str + "\033[0m"
}

func cyan(str string) string {
	return "\033[36m" + str + "\033[0m"
}

func bold(str string) string {
	return "\033[37;1m" + str + "\033[0m"
}
