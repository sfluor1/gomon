package dashboard

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"strings"
	"sync"

	"github.com/gorilla/websocket"
	"github.com/jroimartin/gocui"
	"gitlab.com/sfluor1/gomon/agent/config"
)

// Dashboard struct that contains data about the Dashboard widgets
type Dashboard struct {
	sections           []*section         // The dashboard sections (2 for the metrics and one for the alerts)
	selected           int                // The currently selected section
	websocket          *websocket.Conn    // The websocket connection to interact with the agent
	respTimeThresholds map[string]float64 // All the response time threshold previously defined in the agent configuration file
	search             string             // The search field entered by the client
	mutex              *sync.Mutex        // A mutex for thread safety
}

// section stores the data for a given Section of the Dashboard for example the Alerts list
type section struct {
	name   string         // The Name of the section (used only by gocui)
	offset int            // The scrolling offset of the section
	data   []string       // The data stored by the section
	header string         // The header of the section (table headers for instance)
	title  string         // The title of the section
	config config.Section // The section configuration extracted from the agent configuration
}

// New init the Dashboard and returns it
func New(addr string) *Dashboard {

	// Setup the websocket connection
	url := url.URL{Scheme: "ws", Host: addr}
	c, _, err := websocket.DefaultDialer.Dial(url.String(), nil)
	if err != nil {
		fmt.Fprintf(os.Stderr, "dial: %s\n", err)
		os.Exit(1)
	}

	// Wait for the configuration message
	_, message, err := c.ReadMessage()
	if err != nil {
		fmt.Fprintf(os.Stderr, "read: %s\n", err)
		os.Exit(1)
	}
	var config config.GomonConfig
	err = json.Unmarshal(message, &config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading the config: %s\n", err)
		os.Exit(1)
	}

	dash := buildDashboard(config.Section1, config.Section2, c)

	// Parse response time thresholds and store them for further use
	for _, wc := range config.Watchers {
		if wc.RespTimeThreshold != 0 {
			dash.respTimeThresholds[wc.URL] = wc.RespTimeThreshold
		}
	}

	return dash
}

// Start starts the dashboard mainloop
func (dash *Dashboard) Start() {
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	g.SetManagerFunc(dash.layout)

	go dash.update(g)

	defer g.Close()

	dash.handleEvents(g)
}

func buildDashboard(cf1, cf2 config.Section, con *websocket.Conn) *Dashboard {

	metricsHeader := bold(fmt.Sprintf(
		" %-45.45s | %-8.8s | %-8.8s | %-8.8s | %-10.10s | %s",
		"website",
		"min (s)",
		"avg (s)",
		"max (s)",
		"avlbty (%)",
		"Status Codes Count (code: count)",
	))

	alertsHeader := bold(fmt.Sprintf(" %-20.20s| %s", "Time", "Message"))

	return &Dashboard{
		[]*section{
			&section{"0", 0, []string{}, metricsHeader, formatTimeLabel(1, cf1.TimeRange), cf1},
			&section{"1", 0, []string{}, metricsHeader, formatTimeLabel(2, cf2.TimeRange), cf2},
			&section{"2", 0, []string{}, alertsHeader, "(3) Alerts (Last update: ??:??:??)", config.Section{}},
		},
		2,
		con,
		map[string]float64{},
		"",
		&sync.Mutex{},
	}
}

func (dash *Dashboard) layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	helpPanel := []string{
		"(LEFT)" + cyanBg("Previous Panel"),
		"(RIGHT)" + cyanBg("Next Panel"),
		"(^s)" + cyanBg("Search"),
		"(UP)" + cyanBg("Scroll up"),
		"(DOWN)" + cyanBg("Scroll down"),
		"(^c)" + cyanBg("quit"),
	}

	// Init the dashboard sections
	for _, section := range dash.sections {
		if v, err := g.SetView(section.name, 0, 0, maxX-1, maxY-3); err != nil {
			if err != gocui.ErrUnknownView {
				return err
			}

			v.Title = section.title

			fmt.Fprint(v, section.header)
		}
	}

	// Init the Help panel
	if v, err := g.SetView("help", 0, maxY-3, maxX-1, maxY-1); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}

		v.Frame = false
		fmt.Fprint(v, strings.Join(helpPanel, " | "))
	}

	return nil
}

// reload reloads the whole dashboard (all 3 sections)
func (dash *Dashboard) reload(g *gocui.Gui) {
	g.Update(func(g *gocui.Gui) error {
		for i := 0; i < 3; i++ {
			dash.reloadSection(i, g)
		}

		return nil
	})

}

// reloadSection reloads only one section
func (dash *Dashboard) reloadSection(sec int, g *gocui.Gui) {
	g.Update(func(g *gocui.Gui) error {
		dash.mutex.Lock()

		v, err := g.View(dash.sections[sec].name)
		if err != nil {
			return err
		}
		v.Clear()
		v.Title = dash.sections[sec].title

		show := []string{}

		// No search buffer
		if dash.search == "" {
			show = dash.sections[sec].data
		} else {
			// Search buffer, keep only rows matching the search field
			for _, str := range dash.sections[sec].data {
				if strings.Contains(str, dash.search) {
					show = append(show, str)
				}
			}
		}

		// Fix the offset to avoid being outside the data max range
		offset := min(dash.sections[sec].offset, len(show))

		fmt.Fprint(v, strings.Join(append([]string{dash.sections[sec].header}, show[offset:]...), "\n"))
		dash.mutex.Unlock()

		return nil
	})
}
