package dashboard

import (
	"fmt"
	"log"

	"github.com/jroimartin/gocui"
)

func (dash *Dashboard) handleEvents(g *gocui.Gui) {
	// CTRL-C Exits
	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	// CTRL-S Activate the searchField
	if err := g.SetKeybinding("", gocui.KeyCtrlS, gocui.ModNone, dash.searchField); err != nil {
		log.Panicln(err)
	}

	// Left, Right to Navigate between sections
	if err := g.SetKeybinding("", gocui.KeyArrowLeft, gocui.ModNone, dash.previousView); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("", gocui.KeyArrowRight, gocui.ModNone, dash.nextView); err != nil {
		log.Panicln(err)
	}

	// Up, Down to scroll in a given section
	if err := g.SetKeybinding("", gocui.KeyArrowUp, gocui.ModNone, dash.scrollUp); err != nil {
		log.Panicln(err)
	}
	if err := g.SetKeybinding("", gocui.KeyArrowDown, gocui.ModNone, dash.scrollDown); err != nil {
		log.Panicln(err)
	}

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

func (dash *Dashboard) scrollUp(g *gocui.Gui, v *gocui.View) error {

	dash.mutex.Lock()
	dash.sections[dash.selected].offset = max(dash.sections[dash.selected].offset-1, 0)
	dash.mutex.Unlock()

	dash.reloadSection(dash.selected, g)
	return nil
}

func (dash *Dashboard) scrollDown(g *gocui.Gui, v *gocui.View) error {

	dash.mutex.Lock()
	dash.sections[dash.selected].offset = min(dash.sections[dash.selected].offset+1, len(dash.sections[dash.selected].data))
	dash.mutex.Unlock()

	dash.reloadSection(dash.selected, g)
	return nil
}

func (dash *Dashboard) nextView(g *gocui.Gui, v *gocui.View) error {
	dash.mutex.Lock()
	dash.selected = (dash.selected + 1) % 3
	dash.mutex.Unlock()

	return dash.selectView(g)
}

func (dash *Dashboard) previousView(g *gocui.Gui, v *gocui.View) error {
	dash.mutex.Lock()
	dash.selected = (dash.selected + 2) % 3
	dash.mutex.Unlock()

	return dash.selectView(g)
}

func (dash *Dashboard) selectView(g *gocui.Gui) error {
	dash.mutex.Lock()
	name := fmt.Sprintf("%d", dash.selected)
	dash.mutex.Unlock()

	_, err := g.SetCurrentView(name)

	if err != nil {
		return err
	}
	_, err = g.SetViewOnTop(name)

	return err
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}

func min(a, b int) int {
	if a > b {
		return b
	}

	return a
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
