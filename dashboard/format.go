package dashboard

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/sfluor1/gomon/agent/config"
	"gitlab.com/sfluor1/gomon/agent/db"
	"gitlab.com/sfluor1/gomon/agent/logger"
)

// formatTimeLabel pretty prints the sections time labels
func formatTimeLabel(section int, timeRange uint) string {
	var formatted string
	// Hour
	if timeRange%3600 == 0 {
		if h := timeRange / 3600; h == 1 {
			formatted = "hour"
		} else {
			formatted = fmt.Sprintf("%d hours", h)
		}
		// Minutes
	} else if timeRange%60 == 0 {
		if m := timeRange / 60; m == 1 {
			formatted = "minute"
		} else {
			formatted = fmt.Sprintf("%d minutes", m)
		}
	} else {
		formatted = fmt.Sprintf("%ds", timeRange)
	}
	return fmt.Sprintf("(%d) Stats for the past %s (Last update: ??:??:??)", section, formatted)
}

// formatAlertLog pretty prints the alert records received from the agent
func formatAlertLog(arr []db.AlertRecord, time time.Time) []string {
	res := []string{}
	for _, al := range arr {
		var txt string
		switch al.Type {
		case db.AvbltyRecover:
			{
				txt = fmt.Sprintf("Website %s recovered.", al.URL) + green(fmt.Sprintf(" availability=%.5f %%", al.Availability))
			}
		case db.RTRecover:
			{
				txt = fmt.Sprintf("Website %s response time recovered.", al.URL) + green(fmt.Sprintf(" avg=%.5f (s)", al.RespTime))
			}
		case db.DownAlert:
			{
				txt = fmt.Sprintf("Website %s is down.", al.URL) + red(fmt.Sprintf(" availability=%.5f %%", al.Availability))
			}
		case db.RTAlert:
			{
				txt = fmt.Sprintf("Website %s response time is alerting.", al.URL) + yellow(fmt.Sprintf(" avg=%.5f (s)", al.RespTime))
			}
		}
		res = append(res, fmt.Sprintf(" %v | %s", al.TimeStamp.Format(logger.TimeFormat), txt))
	}
	return res
}

func formatMetrics(m db.Metrics, respTimeCritical bool) string {
	rowColor := green
	respTimeColor := green
	if respTimeCritical {
		respTimeColor = yellow
	}
	if m.Availability < config.AVBLTHRESHOLD {
		rowColor = red
		respTimeColor = red
	}
	codes, _ := json.Marshal(m.StatusCodes)
	return rowColor(fmt.Sprintf(" %-45.45s", m.URL)) +
		respTimeColor(fmt.Sprintf(" | %-8.4f | %-8.4f | %-8.4f", m.MinResponseTime, m.AvgResponseTime, m.MaxResponseTime)) +
		rowColor(fmt.Sprintf(" | %-10.4f | %v", m.Availability, strings.Trim(strings.Replace(string(codes), "\"", "", -1), "{}")))
}
