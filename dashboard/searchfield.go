package dashboard

import (
	"strings"

	"github.com/jroimartin/gocui"
)

// searchField triggers the searchField and a new keybinding for the ENTER key
func (dash *Dashboard) searchField(g *gocui.Gui, v *gocui.View) error {
	maxX, maxY := g.Size()
	title := "Search a website"
	name := "search"

	if sv, err := g.SetView(name, maxX/2-12, maxY/2, maxX/2+12, maxY/2+2); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		sv.Title = title
		sv.Editable = true
		g.Cursor = true

		if _, err := g.SetCurrentView(name); err != nil {
			return err
		}
		// If ENTER is pressed we launch copyInput
		if err := g.SetKeybinding(name, gocui.KeyEnter, gocui.ModNone, dash.copyInput); err != nil {
			return err
		}
	}
	return nil
}

// copyInput is executed when the user hits ENTER while in the search field, it records the search buffer and closes the search input
func (dash *Dashboard) copyInput(g *gocui.Gui, sv *gocui.View) error {
	// We want to read the view’s buffer from the beginning.
	sv.Rewind()

	// Must delete keybindings before the view, or fatal error
	g.DeleteKeybindings(sv.Name())
	if err := g.DeleteView(sv.Name()); err != nil {
		return err
	}

	g.Cursor = false

	dash.mutex.Lock()
	dash.search = strings.TrimSpace(sv.Buffer())
	dash.mutex.Unlock()

	dash.reload(g)

	return nil
}
