package dashboard

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/jroimartin/gocui"

	"gitlab.com/sfluor1/gomon/agent/alerter"
)

const headerTimeFormat = "(Last update: 15:04:05)"

// update is the dashboard message loop, it reads new messages from the agent from the websocket connection
func (dash *Dashboard) update(g *gocui.Gui) {
	for {
		_, data, err := dash.websocket.ReadMessage()
		if err != nil {
			fmt.Fprintln(os.Stderr, "read error:", err)
			os.Exit(1)
		}
		var msg alerter.Message
		json.Unmarshal(data, &msg)

		dash.handleMessage(msg, g)
	}
}

func (dash *Dashboard) handleMessage(msg alerter.Message, g *gocui.Gui) {
	switch msg.Type {
	case alerter.SECTION1UPDATE:
		{
			dash.updateMetricsSection(0, msg, dash.respTimeThresholds, g)
		}
	case alerter.SECTION2UPDATE:
		{
			dash.updateMetricsSection(1, msg, dash.respTimeThresholds, g)
		}
	case alerter.ALRTSECTIONUPDATE:
		{
			dash.mutex.Lock()
			dash.sections[2].data = append(formatAlertLog(msg.Alerts, msg.TimeStamp), dash.sections[2].data...)
			dash.mutex.Unlock()
			dash.updateSectionTimeLabel(2, msg.TimeStamp, g)
		}
	case alerter.PREVIOUSALERTS:
		{
			dash.mutex.Lock()
			dash.sections[2].data = reverse(formatAlertLog(msg.Alerts, msg.TimeStamp))
			dash.mutex.Unlock()
			dash.updateSectionTimeLabel(2, msg.TimeStamp, g)
		}
	default:
		{
			fmt.Fprintf(os.Stderr, "Unknown message type: %v, %v\n", msg.Type, msg)
			os.Exit(1)
		}
	}

	dash.reload(g)
}

func (dash *Dashboard) updateSectionTimeLabel(sec int, t time.Time, g *gocui.Gui) {
	dash.mutex.Lock()
	dash.sections[sec].title = dash.sections[sec].title[:len(dash.sections[sec].title)-len(headerTimeFormat)] + t.Format(headerTimeFormat)
	dash.mutex.Unlock()
	dash.reloadSection(sec, g)
}

func (dash *Dashboard) updateMetricsSection(sec int, msg alerter.Message, thresholds map[string]float64, g *gocui.Gui) {
	met := msg.Metrics
	data := []string{}
	for _, p := range met {
		data = append(data, formatMetrics(p, thresholds[p.URL] != 0 && thresholds[p.URL] < p.AvgResponseTime))
	}
	dash.mutex.Lock()
	dash.sections[sec].data = data
	dash.mutex.Unlock()

	dash.updateSectionTimeLabel(sec, msg.TimeStamp, g)
}

func reverse(arr []string) []string {
	res := []string{}
	l := len(arr)

	for i := range arr {
		res = append(res, arr[l-i-1])
	}

	return res
}
