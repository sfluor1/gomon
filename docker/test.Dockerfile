FROM golang:1.10 AS builder

RUN curl -fsSL -o /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 && chmod +x /usr/local/bin/dep

RUN mkdir -p /go/src/gitlab.com/sfluor1/gomon
WORKDIR /go/src/gitlab.com/sfluor1/gomon
COPY . .
RUN dep ensure
