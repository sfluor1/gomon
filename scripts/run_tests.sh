#!/bin/bash
cd "$(dirname "$0")"

docker-compose -f ../docker/test.yml build
docker-compose -f ../docker/test.yml run --rm tester go test $(go list ../... | grep -v /vendor/)
docker-compose -f ../docker/test.yml stop